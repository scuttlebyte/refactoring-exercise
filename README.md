## Requirements
- php 7.1+
- composer

## Installation
From the project root directory

    composer install --dev
    
This will install package dependencies
    
## Serving the demo application

    composer serve
    
This will serve the application using the built-in php server. 

You can access the demo page at [http://localhost:3099/public/index.php](http://localhost:3099/public/index.php)
    
## Tests

From the project root directory, use the following helpers to run the test suites:

Run all tests:
    
    composer test 

Run integration/feature tests:
    
    composer feature
    
Run unit tests:

    composer unit
    
## Exercise strategy

This section will cover the general approach for the refactoring exercise, as well as caveats and examples of how the application could be further improved. 

### General approach

The project 

The goals for the refactor (in order of priority) were:

- Reduce risk of making changes to the application
- Maintain existing API A.K.A don't break backwards compatibility
- Initiate a test suite to:
    - Ensure backwards compatibility 
    - Improve domain understanding
    - Document project behaviour
- Improve maintainablity through "self-documenting," **SOLID** services and methods
- Add interfaces where appropriate for features likely to change in the future
- Lay the foundation for new features by way of polymorphism
 
The refactoring process entailed the following activities (some minor actions have been omitted for brevity). For a more detailed explanation, see the commit log.

1. Wrapped the existing `Customer::statement()` API in a newly created integration test
2. Normalized class architecture and verified tests pass, e.g. moved classes to `src/` directory
3. Moved statement generation functionality to dedicated services, e.g. `App\Statement\Generator`
4. Extracted output processing to a dedicated formatter class, `App\Statment\Formatter\PlainTextFormatter`
7. Created HTML formatter for report system
5. Refactored `Movie` "category" metadata to dedicated Domain object, `App\MovieCategory`
6. Refactored Loyalty and Rental cost calculations to dedicated, unit-tested services and applicable helper methods

### Important caveats

* Existing application APIs were left intact to maintain Backwards Compatibility, although they may break traditional SOLID principles
* The requirements explicitly required a method added to the client method, although all business logic was extracted to dedicated services, and this method serves purely as a proxy/convenience method to access those services.
* There is no Service Container, so many objects are instantiated in inopportune locations. Typically I would rely heavily on Dependency Injection
* Added a collection dependency, as the convenience helpers are helpful in reducing boilerplate code 

### Future Improvements

These changes would have been made, had time permitted, and had they fallen within the scope of the exercise.

**Service Container** - Without a fully-featured IoC container, e.g. [Pimple](https://pimple.symfony.com/), the configuration of the application's services can become tiresome, and more importantly error-prone.

**Further rental cost calculation extraction** - Right now, the calculation for a given rental's cost is primarily contained within the `\App\Rental\Domain\Rental` class, but should be extracted to a dedicated service like the `App\Loyalty\Calculator\LoyaltyPointCalculator`

**Documentation** - In-code documentation (docblocks) were used on most new code, although there is certainly room for improvement. Ideally documentation would be extracted from docblocks and deployed to standalone documentation. Additionally, class and flow diagrams would typically be provided with any major refactoring or architecture expansion effort.   

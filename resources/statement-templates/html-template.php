<h1>Rental Record for <em><?= $this->e($name) ?></em></h1>
<ul>
<?php foreach ($rentals as $rental): ?>
    <li><?= $this->e($rental->movie()->name()) ?> - <?= $this->e($rental->getTotalCost()) ?></li>
<?php endforeach ?>
</ul>
<p>Amount owed is <em><?= $this->e($amountOwed) ?></em>
<p>You earned <em><?= $this->e($pointsEarned) ?></em> frequent renter points</p>

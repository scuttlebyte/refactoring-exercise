<?php

return [
    0 => [
        'name' => 'regular',
        'duration' => 2,
        'baseCost' => 2,
        'additionalDayCost' => 1.5,
    ],

    1 => [
        'name' => 'new_release',
        'duration' => 1,
        'baseCost' => 3,
        'additionalDayCost' => 3,
    ],

    2 => [
        'name' => 'childrens',
        'duration' => 3,
        'baseCost' => 1.5,
        'additionalDayCost' => 1.5,
    ],

    3 => [
        'name' => 'video_game',
        'duration' => 6,
        'baseCost' => 10,
        'additionalDayCost' => 2,
    ],
];

<?php namespace App\Tests\Unit;

use App\Customer\Domain\Customer;
use App\Movie\Domain\Movie;
use App\Rental\Domain\Rental;

class CustomerTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @return array
     */
    public function rentalsDataProvider()
    {
        return [
            [
                [
                    $rental1 = new Rental(
                        new Movie(
                            'Back to the Future',
                            Movie::CHILDRENS
                        ), 4
                    ),

                    $rental2 = new Rental(
                        new Movie(
                            'Office Space',
                            Movie::REGULAR
                        ), 3
                    ),

                    $rental3 = new Rental(
                        new Movie(
                            'The Big Lebowski',
                            Movie::NEW_RELEASE
                        ), 5
                    )
                ]
            ]
        ];
    }

    /**
     * @test
     */
    public function set_and_get_the_name()
    {
        // Arrange
        // Act
        $customer = new Customer('John Smith');

        // Assert
        $this->assertEquals('John Smith', $customer->name());
    }

    /**
     * @test
     * @dataProvider rentalsDataProvider
     * @param array $rentals
     */
    public function generate_plain_text_statement($rentals)
    {
        // Arrange
        $customer = new Customer('Joe Schmoe');

        foreach ($rentals as $rental) {
            $customer->addRental($rental);
        }

        // Act
        $statement = $customer->statement();

        // Assert
        self::assertEquals(static::plainTextStatement(), $statement);
    }


    /**
     * @test
     * @dataProvider rentalsDataProvider
     * @param array $rentals
     */
    public function generate_html_statement($rentals)
    {
        // Arrange
        $customer = new Customer('Joe Schmoe');

        foreach ($rentals as $rental) {
            $customer->addRental($rental);
        }

        // Act
        $statement = $customer->htmlStatement();

        // Assert
        self::assertEquals(static::htmlStatement(), $statement);
    }

    /**
     * @return string
     */
    private static function plainTextStatement()
    {
        // Odd formatting here is intentional
        return <<<STATEMENT
Rental Record for Joe Schmoe
	Back to the Future            	3
	Office Space                  	3.5
	The Big Lebowski              	15
Amount owed is 21.5
You earned 4 frequent renter points

STATEMENT;
    }

    private static function htmlStatement()
    {
        // Odd formatting here is intentional
        return <<<STATEMENT
<h1>Rental Record for <em>Joe Schmoe</em></h1>
<ul>
    <li>Back to the Future - 3</li>
    <li>Office Space - 3.5</li>
    <li>The Big Lebowski - 15</li>
</ul>
<p>Amount owed is <em>21.5</em>
<p>You earned <em>4</em> frequent renter points</p>

STATEMENT;
    }
}

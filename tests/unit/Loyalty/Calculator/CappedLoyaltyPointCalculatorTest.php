<?php


use App\Loyalty\Calculator\CappedLoyaltyPointCalculator;
use App\Movie\Domain\Movie;
use App\Rental\Domain\Rental;

class CappedLoyaltyPointCalculatorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     * @dataProvider dataProvider
     * @param Rental $rental
     * @param int $expectation
     */
    public function it_calculates(Rental $rental, int $expectation)
    {
        // Arrange
        $sut = new CappedLoyaltyPointCalculator();

        // Act
        $actual = $sut->forRental(
            $rental
        );

        // Assert

        self::assertEquals($expectation, $actual);
    }

    public function dataProvider()
    {
        // We don't bother mocking these because they're just POPOs
        return [
            'a capped value' => [
                new \App\Rental\Domain\Rental(
                    new Movie(
                        'Bill & Ted\'s Excellent Adventure',
                        Movie::NEW_RELEASE
                    ), 99
                ),
                10
            ],
            'an uncapped value' => [
                    new \App\Rental\Domain\Rental(
                        new Movie(
                        'Bill & Ted\'s Excellent Adventure',
                        Movie::NEW_RELEASE
                    ), 1
                ),
                5
            ],
            'a childrens release' => [
                new \App\Rental\Domain\Rental(
                    new Movie(
                        'The LEGO Movie',
                        Movie::CHILDRENS
                    ), 1
                ),
                5
            ]
        ];
    }
}

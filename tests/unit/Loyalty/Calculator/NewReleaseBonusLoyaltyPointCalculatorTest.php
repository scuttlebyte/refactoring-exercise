<?php


use App\Loyalty\Calculator\NewReleaseBonusLoyaltyPointCalculator;
use App\Movie\Domain\Movie;
use App\Rental\Domain\Rental;

class NewReleaseBonusLoyaltyPointCalculatorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     * @dataProvider dataProvider
     * @param Rental $rental
     * @param int $expectation
     */
    public function it_calculates(Rental $rental, int $expectation)
    {
        // Arrange
        $sut = new NewReleaseBonusLoyaltyPointCalculator();

        // Act
        $actual = $sut->forRental(
            $rental
        );

        // Assert

        self::assertEquals($expectation, $actual);
    }

    public function dataProvider()
    {
        // We don't bother mocking these because they're just POPOs
        return [
            'a new release' => [
                new \App\Rental\Domain\Rental(
                    new Movie(
                        'Bill & Ted\'s Excellent Adventure',
                        Movie::NEW_RELEASE
                    ), 99
                ),
                2
            ],
            'a new release with one day' => [
                new \App\Rental\Domain\Rental(
                    new Movie(
                        'Bill & Ted\'s Excellent Adventure',
                        Movie::NEW_RELEASE
                    ), 1
                ),
                1
            ],
            'a childrens release' => [
                new \App\Rental\Domain\Rental(
                    new Movie(
                        'The LEGO Movie',
                        Movie::CHILDRENS
                    ), 99
                ),
                1
            ]
        ];
    }
}

<?php

use App\Customer\Domain\Customer;
use App\Movie\Domain\Movie;
use App\Rental\Domain\Rental;

require_once __DIR__ . '/../vendor/autoload.php';

$rental1 = new Rental(
    new Movie(
        'Back to the Future',
        Movie::CHILDRENS
    ), 4
);

$rental2 = new Rental(
    new Movie(
        'Office Space',
        Movie::REGULAR
    ), 3
);

$rental3 = new Rental(
    new Movie(
        'The Big Lebowski',
        Movie::NEW_RELEASE
    ), 5
);

$customer = new Customer('Joe Schmoe');

$customer->addRental($rental1);
$customer->addRental($rental2);
$customer->addRental($rental3);
?>

<html>
    <pre>
        <?php echo $customer->statement(); ?>
    </pre>
    <hr>
    <div>
        <?php echo $customer->htmlStatement(); ?>
    </div>
</html>

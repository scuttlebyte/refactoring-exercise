<?php

namespace App\Statement\Formatter;

use App\Movie\Domain\Movie;
use App\Rental\Domain\Rental;
use App\Statement\Domain\Statement;

class PlainTextFormatter implements Formatter
{
    public function format(Statement $statement): string
    {
        $result = 'Rental Record for ' . $statement->getName() . PHP_EOL;
        /** @var Rental $rental */
        foreach ($statement->getRentals() as $rental) {
            $result .= "\t" . str_pad($rental->movie()->name(), 30, ' ', STR_PAD_RIGHT) . "\t" . $rental->getTotalCost() . PHP_EOL;
        }

        $result .= 'Amount owed is ' . $statement->getAmountOwed() . PHP_EOL;
        $result .= 'You earned ' . $statement->getPointsEarned() . ' frequent renter points' . PHP_EOL;

        return $result;
    }
}

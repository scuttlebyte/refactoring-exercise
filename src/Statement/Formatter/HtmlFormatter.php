<?php

namespace App\Statement\Formatter;

use App\Statement\Domain\Statement;
use League\Plates\Engine;

class HtmlFormatter implements Formatter
{
    public function format(Statement $statement): string
    {
        $engine = new Engine( __DIR__ . '/../../../resources/statement-templates');

        return $engine->render('html-template', $statement->toArray());
    }
}

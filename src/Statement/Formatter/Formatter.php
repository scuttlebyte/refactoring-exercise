<?php

namespace App\Statement\Formatter;

use App\Statement\Domain\Statement;

interface Formatter
{
    public function format(Statement $statement): string;
}

<?php

namespace App\Statement\Generator;

use App\Customer\Domain\Customer;
use App\Statement\Domain\Statement;
use App\Statement\Formatter\Formatter;
use App\Statement\Hydrator\StatementHydrator;

class Generator {
    /**
     * @var StatementHydrator
     */
    private $hydrator;
    /**
     * @var Formatter
     */
    private $formatter;

    /**
     * Generator constructor.
     * @param StatementHydrator $hydrator
     * @param Formatter $formatter
     */
    public function __construct(StatementHydrator $hydrator, Formatter $formatter)
    {
        $this->hydrator = $hydrator;
        $this->formatter = $formatter;
    }


    /**
     * Generate a statement for a given user
     * @param Customer $customer
     * @return string
     */
    public function generateStatement(Customer $customer)
    {
        $statement = $this->hydrator->hydrate(new Statement(), $customer);

        return $this->formatter->format($statement);
    }
}

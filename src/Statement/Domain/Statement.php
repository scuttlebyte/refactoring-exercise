<?php

namespace App\Statement\Domain;

use App\Customer\Domain\Customer;
use App\Loyalty\Calculator\NewReleaseBonusLoyaltyPointCalculator;
use App\Rental\Domain\Rental;
use Tightenco\Collect\Support\Collection;

class Statement
{
    /**
     * @var Customer
     */
    private $customer;
    /**
     * @var Collection
     */
    private $rentals;

    public function setCustomer(\App\Customer\Domain\Customer $customer)
    {
        $this->customer = $customer;
    }

    private function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return Collection
     */
    public function getRentals(): Collection
    {
        return $this->rentals;
    }

    /**
     * @param Collection $rentals
     */
    public function setRentals(Collection $rentals): void
    {
        $this->rentals = $rentals;
    }

    public function getName(): string
    {
        return $this->getCustomer()->getName();
    }

    /**
     * @return float
     */
    public function getAmountOwed(): float
    {
        return $this->getRentals()->sum(function(Rental $rental) {
            return $rental->getTotalCost();
        });
    }

    /**
     * @return int
     */
    public function getPointsEarned(): int
    {
        $calculator = new NewReleaseBonusLoyaltyPointCalculator();
        return $this->getRentals()->sum(function (Rental $rental) use ($calculator) {
            return $calculator->forRental($rental);
        });
    }

    public function toArray()
    {
        return [
            'name' => $this->getName(),
            'amountOwed' => $this->getAmountOwed(),
            'pointsEarned' => $this->getPointsEarned(),
            'rentals' => $this->getRentals(),
        ];
    }

}

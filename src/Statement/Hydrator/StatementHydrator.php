<?php

namespace App\Statement\Hydrator;

use App\Customer\Domain\Customer;
use App\Statement\Domain\Statement;

class StatementHydrator
{
    public function hydrate(Statement $statement, Customer $customer): Statement
    {
        $statement->setCustomer($customer);
        $statement->setRentals($customer->getRentals());

        return $statement;
    }
}

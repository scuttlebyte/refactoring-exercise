<?php namespace App\Customer\Domain;

use App\Movie\Domain\Movie;
use App\Rental\Domain\Rental;
use App\Statement\Domain\Statement;
use App\Statement\Formatter\HtmlFormatter;
use App\Statement\Formatter\PlainTextFormatter;
use App\Statement\Generator\Generator;
use App\Statement\Hydrator\StatementHydrator;
use Tightenco\Collect\Support\Collection;

class Customer
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Collection[Rentals]
     */
    private $rentals;

    /**
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->rentals = new Collection();
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->getName();
    }

    /**
     * @param Rental $rental
     */
    public function addRental(Rental $rental)
    {
        $this->rentals->push($rental);
    }

    public function getRentals()
    {
        return $this->rentals;
    }

    public function getName()
    {
        return $this->name;
    }

    public function statement()
    {
        $generator = new Generator(new StatementHydrator, new PlainTextFormatter);

        return $generator->generateStatement($this);
    }

    public function htmlStatement()
    {
        $generator = new Generator(new StatementHydrator, new HtmlFormatter);

        return $generator->generateStatement($this);
    }
}

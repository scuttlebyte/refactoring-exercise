<?php

namespace App\Loyalty\Calculator;

use App\Rental\Domain\Rental;

/**
 * This system will award bonus points at a higher rate per order, but
 * also contains a maximum amount of bonus points available per
 * Rental.
 */
class CappedLoyaltyPointCalculator implements LoyaltyPointCalculator
{
    /**
     * How many base points should be awarded for a rental
     */
    const BASE_REWARD_POINTS = 5;
    /**
     * How many bonus points should be rewarded if the criteria are met
     */
    const ADDITIONAL_BONUS_POINTS_PER_DAY = 1;
    /**
     * The maximum number of points a Customer can earn for any Rental
     */
    const MAXIMUM_LOYALTY_POINTS_ALLOWED = 10;
    /**
     * Minimum number of days the rental should be to qualify for bonus points
     */
    const MINIMUM_RENTAL_DAYS = 2;

    /**
     * @param Rental $rental
     * @return int
     */
    public function forRental(Rental $rental)
    {
        return min(
            (self::BASE_REWARD_POINTS + $this->getBonusRewardPoints($rental)) ?? 0,
            self::MAXIMUM_LOYALTY_POINTS_ALLOWED
        );
    }

    /**
     * @param Rental $rental
     * @return int
     */
    private function getBonusRewardPoints(Rental $rental)
    {
        return $rental->daysRented() > self::MINIMUM_RENTAL_DAYS
            ? $rental->daysRented() * self::ADDITIONAL_BONUS_POINTS_PER_DAY
            : 0;
    }
}

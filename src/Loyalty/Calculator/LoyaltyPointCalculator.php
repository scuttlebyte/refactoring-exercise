<?php

namespace App\Loyalty\Calculator;

use App\Rental\Domain\Rental;

interface LoyaltyPointCalculator
{
    /**
     * Calculates the number of loyalty points earned for a given Rental
     * @param Rental $rental
     * @return int
     */
    public function forRental(Rental $rental);
}

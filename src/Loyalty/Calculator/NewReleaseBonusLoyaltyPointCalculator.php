<?php

namespace App\Loyalty\Calculator;

use App\Movie\Domain\Movie;
use App\Rental\Domain\Rental;

/**
 * This system will award bonus points for new releases which are kept longer
 */
class NewReleaseBonusLoyaltyPointCalculator implements LoyaltyPointCalculator
{
    /**
     * How many base points should be awarded for a rental
     */
    const BASE_REWARD_POINTS = 1;
    /**
     * How many bonus points should be rewarded for the convention
     */
    const ADDITIONAL_BONUS_POINTS = 1;
    /**
     * Minimum number of days the rental should be to qualify for bonus points
     */
    const MINIMUM_RENTAL_DAYS = 2;

    public function forRental(Rental $rental)
    {
        return self::BASE_REWARD_POINTS + $this->getBonusRewardPoints($rental);

    }

    private function getBonusRewardPoints(Rental $rental)
    {
        return $rental->movie()->priceCode() === Movie::NEW_RELEASE && $rental->daysRented() >= self::MINIMUM_RENTAL_DAYS
            ? self::ADDITIONAL_BONUS_POINTS
            : 0;
    }
}

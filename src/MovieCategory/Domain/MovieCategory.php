<?php

namespace App\MovieCategory\Domain;

class MovieCategory
{
    /**
     * @var float
     */
    private $additionalDayCost;
    /**
     * @var float
     */
    private $baseCost;
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $duration;

    /**
     * MovieCategory constructor.
     * @param string $name
     * @param int $rentalDays
     * @param float $dailyCost
     * @param float $additionalDayCost
     */
    public function __construct(string $name, int $rentalDays, float $dailyCost, float $additionalDayCost)
    {
        $this->name = $name;
        $this->duration = $rentalDays;
        $this->baseCost = $dailyCost;
        $this->additionalDayCost = $additionalDayCost;
    }

    /**
     * @return float
     */
    public function getAdditionalDayCost(): float
    {
        return $this->additionalDayCost;
    }

    /**
     * @param float $additionalDayCost
     */
    public function setAdditionalDayCost(float $additionalDayCost): void
    {
        $this->additionalDayCost = $additionalDayCost;
    }

    /**
     * @return float
     */
    public function getBaseCost(): float
    {
        return $this->baseCost;
    }

    /**
     * @param float $baseCost
     */
    public function setBaseCost(float $baseCost): void
    {
        $this->baseCost = $baseCost;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getRentalDuration(): int
    {
        return $this->duration;
    }

    /**
     * @param int $duration
     */
    public function setDuration(int $duration): void
    {
        $this->duration = $duration;
    }

    public static function getInstance($code)
    {
        $categories = include __DIR__ . '/../../../config/movieCategories.php';

        if (!array_key_exists($code, $categories)) {
            throw new \InvalidArgumentException('No such category: ' . $code);
        }

        $category = $categories[$code];

        return new static(
            $category['name'],
            $category['duration'],
            $category['baseCost'],
            $category['additionalDayCost']
        );
    }
}

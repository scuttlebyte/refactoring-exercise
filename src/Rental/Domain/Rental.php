<?php namespace App\Rental\Domain;

use App\Movie\Domain\Movie;

class Rental
{
    const BASE_REWARD_POINTS = 1;
    const NEW_RELEASE_ADDITIONAL_DAYS_BONUS_POINTS = 1;

    /**
     * @var Movie
     */
    private $movie;

    /**
     * @var int
     */
    private $daysRented;

    /**
     * @param Movie $movie
     * @param int $daysRented
     */
    public function __construct(Movie $movie, $daysRented)
    {
        $this->movie = $movie;
        $this->daysRented = $daysRented;
    }

    /**
     * @return Movie
     */
    public function movie()
    {
        return $this->movie;
    }

    /**
     * @return int
     */
    public function daysRented()
    {
        return $this->daysRented;
    }

    public function getTotalCost()
    {
        return $this->getBaseCost() + $this->getAdditionalDaysCost();
    }

    private function getBaseCost()
    {
        return $this->movie()->getCategory()->getBaseCost();
    }

    private function getAdditionalDaysCost(): float
    {
        return $this->movie()->getCategory()->getAdditionalDayCost() * $this->getDaysOverdue();
    }

    private function getDaysOverdue()
    {
        return max(
            $this->daysRented() - $this->movie()->getCategory()->getRentalDuration(),
            0
        );
    }
}

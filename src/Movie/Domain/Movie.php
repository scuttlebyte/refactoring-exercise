<?php namespace App\Movie\Domain;

use App\MovieCategory\Domain\MovieCategory;

class Movie
{
    const CHILDRENS = 2;
    const REGULAR = 0;
    const NEW_RELEASE = 1;
    const VIDEO_GAME = 3;
    /**
     * @var MovieCategory
     */
    public $category;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $priceCode;

    /**
     * @param string $name
     * @param int $priceCode
     */
    public function __construct($name, $priceCode)
    {
        $this->name = $name;
        $this->priceCode = $priceCode;
        $this->category = MovieCategory::getInstance($priceCode);
    }

    /**
     * @return string
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function priceCode()
    {
        return $this->priceCode;
    }

    /**
     * @return MovieCategory
     */
    public function getCategory(): MovieCategory
    {
        return $this->category;
    }

    /**
     * @param MovieCategory $category
     */
    public function setCategory(MovieCategory $category): void
    {
        $this->category = $category;
    }
}
